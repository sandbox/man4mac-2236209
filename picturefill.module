<?php



/**
 * Implements hook_theme
 * @return Array  new theme items
 */
function picturefill_theme($existing, $type, $theme, $path){

  $items['picturefill'] = array(
    'template' => 'picturefill',
    'variables' => array(
      'breakpoints' => null,
      'fallback' => null,
      'initial' => null,
      'alt' => null,
    )
  );

  return $items;
}



/**
 * Implements hook_init
 * @todo  Ideally we wouldn't use hook_init here, but the output of the formatter
 *        gets cached so it only executes once. This could be cleaned up
 * @return none
 */
function picturefill_init() {
  libraries_load('picturefill');
}





/**
 * Implements hook_libraries_info().
 */
function picturefill_libraries_info() {

  $libraries['picturefill'] = array(
    'name' => 'Picturefill.js',
    'vendor url' => 'https://github.com/scottjehl/picturefill',
    'download url' => 'https://github.com/scottjehl/picturefill/archive/1.2.1.zip',
    'version arguments' => array(
      'file' => 'bower.json',
      'pattern' => '/(\d+\.\d+\.?\d+?)+/',
      'lines' => 5,
    ),
    'files' => array(
      'js' => array('picturefill.js'),
    ),
  );

  return $libraries;
}



/**
 * Initiate the picturefill file format
 * @return Array  The instantiation array for our picturefill formatter
 */
function picturefill_file_formatter_info() {
  $formatters['picturefill'] = array(
    'label' => t('Picturefill Image'),
    'description' => t('Use the popular <a href="https://github.com/scottjehl/picturefill">picturefill.js</a> pollyfill to render images responsively'),
    'file types' => array('image'),
    'default settings' => array(
      'fallback' => 'large',
      'initial' => 'medium'
    ),
    'view callback' => 'picturefill_file_formatter_picturefill_view',
    'settings callback' => 'picturefill_file_formatter_picturefill_settings',
    'mime types' => array('image/*'),
  );

  return $formatters;  
}




/**
 * Implements hook_file_formatter_FORMATTER_settings
 * @return none  modification performed by reference
 */
function picturefill_file_formatter_picturefill_settings($form, &$form_state, $settings) {
  $element = $styleOptions = array();
  $breakpoints = 5;

  $styles = image_styles();

  foreach ($styles as $key => $options) {
    $styleOptions[$key] = $options['label'];
  }
  $element['fallback'] = array(
    '#type' => 'select',
    '#title' => t('Fallback Size'),
    '#description' => 'Image style to use if the browser has JavaScript disabled',
    '#default_value' => @$settings['fallback'],
    '#options' => $styleOptions
  );

  $element['initial'] = array(
    '#type' => 'select',
    '#title' => t('Initial Size'),
    '#description' => 'Initial image to load, generally this should be a small version',
    '#default_value' => @$settings['initial'],
    '#options' => $styleOptions
  );

  for ($i=0; $i<$breakpoints; $i++) {
    $element['breakpoints'][$i] = array(
      '#type' => 'fieldset',
    );
    $element['breakpoints'][$i]['size'] = array(
      '#type' => 'textfield',
      '#title' => 'Breakpoint',
      '#description' => 'Include the breakpoint @media information. Example: min-width: 400px',
      '#default_value' => @$settings['breakpoints'][$i]['size']
    );

    $element['breakpoints'][$i]['style'] = array(
      '#type' => 'select',
      '#title' => 'Image Style',
      '#options' => $styleOptions,
      '#description' => 'Image style for this breakpoint',
      '#default_value' => @$settings['breakpoints'][$i]['style']
    );

  }


  return $element;
}



/**
 * Implements hook_file_formatter_FORMATTER_view().
 * @return none
 */
function picturefill_file_formatter_picturefill_view($file, $display, $langcode){
  $breakpoints = array();


  if ($file->type == 'image') {
    // Loop through each setting and create a breakpoint
    foreach ($display['settings']['breakpoints'] as $breakpoint) {
      if (isset($breakpoint['size']) && $breakpoint['size']) {
        $breakpoints[] = array(
          'media' => $breakpoint['size'],
          'url' => image_style_url($breakpoint['style'], $file->uri)
        );
      }
    }
    $output = array(
      '#theme' => 'picturefill',
      '#breakpoints' => $breakpoints,
      '#file' => $file,
      '#fallback' => image_style_url($display['settings']['fallback'], $file->uri),
      '#initial' => image_style_url($display['settings']['initial'], $file->uri),
      '#alt' => $file->alt,
    );


    return $output;
  }

  return false;
}