<span data-picture data-alt="<?php echo $alt; ?>">
    <span data-src="<?php echo $initial ?>"></span>
    <? foreach ($breakpoints as $breakpoint): ?>
      <span data-src="<?php echo $breakpoint['url'] ?>" data-media="(<?= $breakpoint['media'] ?>)"></span>
    <? endforeach; ?>
    <noscript>
        <img src="<?php echo $fallback; ?>" alt="<?php echo $alt; ?>">
    </noscript>
</span>